#include "base/kaldi-common.h"
#include "util/common-utils.h"
#include "nnet3/nnet-am-decodable-simple.h"
#include "base/timer.h"
#include "nnet3/nnet-utils.h"
#include "ivector/agglomerative-clustering.h"
#include "json.h"

#ifndef KALDI_CUDA_DECODER_BIN_CUDA_SPEAKER_DIARIZATION_TOOLS_H_
#define KALDI_CUDA_DECODER_BIN_CUDA_SPEAKER_DIARIZATION_TOOLS_H_


namespace kaldi {
namespace cuda_decoder {

using namespace nnet3;


struct BatchedXvectorComputerOptions {
    int32 chunk_size{ 150 };
    int32 batch_size{ 32 };
    bool pad_input{ true };
    float target_energy{ 0.1 };
    int32 num_speakers{ -1 };
    NnetComputeOptions compute_config;
    NnetOptimizeOptions optimize_config;
    CachingOptimizingCompilerOptions compiler_config;


    void Register(OptionsItf* po) {
        po->Register("chunk-size", &chunk_size,
            "Size of chunk, in input frames.  Includes the nnet "
            "context, so the number of chunks will be more than "
            "total-input-frames / chunk-size.");
        po->Register("target-energy", &target_energy,
            "target engery for PCA analysis.");
        po->Register("num-speakers", &num_speakers,
            "known number of speakers for speaker diarization.");
        po->Register("xvector-batch-size", &batch_size,
            "Size of the batches of chunks that we compute at once. ");
        po->Register("pad-input", &pad_input,
            "If true, for utterances shorter than `chunk-size` frames "
            "we will pad with repeats of the last frame.");
        compute_config.Register(po);
        optimize_config.Register(po);
        compiler_config.Register(po);
    }
};


/**
    This function divides the number 'a' into 'b' pieces, such that
    the sum of the pieces equals 'a' and no two pieces differ by more
    than 1.
        @param [in] a     A number, may be positive or negative
        @param [in] b     The number of pieces, b >= 1.
        @param [out] pieces   The pieces will be written to here.
                        At exit, their sum will equal a, and none
                        of them will differ from any other by more
                        than 1.  Otherwise they are arbitrarily
                        chosen.
    */
void DivideIntoPieces(int32 a, int32 b, std::vector<int32>* pieces) {
    KALDI_ASSERT(b > 0);
    pieces->clear();
    pieces->reserve(b);
    int32 a_sign = 1;
    // Make sure a is positive before division, because the behavior of division
    // with negative operands is not fully defined in C.
    if (a < 0) {
        a_sign = -1;
        a *= -1;
    }
    int32 piece_size1 = a / b,
        piece_size2 = piece_size1 + 1,
        remainder = a % b;
    int32 num_pieces_of_size1 = b - remainder,
        num_pieces_of_size2 = remainder;
    KALDI_ASSERT(a == num_pieces_of_size1 * piece_size1 +
        num_pieces_of_size2 * piece_size2);

    for (int32 i = 0; i < num_pieces_of_size1; i++)
        pieces->push_back(piece_size1 * a_sign);
    for (int32 i = 0; i < num_pieces_of_size2; i++)
        pieces->push_back(piece_size2 * a_sign);
}


class BatchedXvectorComputer {
public:
    /**
            @param [in]  opts  Options class; warning, it keeps a reference to it.
            @param [in]  nnet  The neural net we'll be computing with; assumed to have
                            already been prepared for test.
            @param [in] total_context   The sum of the left and right context of the
                            network, computed after calling
                            SetRequireDirectInput(true, &nnet); so the l/r context
                            isn't zero.
        */

    BatchedXvectorComputer(const BatchedXvectorComputerOptions& opts,
        const Nnet& nnet,
        int32 total_context);

    /**
        Accepts an utterance to process into an xvector, and, if one or more
        batches become full, processes the batch.
        */
    void AcceptUtterance(const std::string& utt,
        const Matrix<BaseFloat>& input);


    /**  Returns true if at least one xvector is pending output (i.e. that
            the user may call OutputXvector()).
        */
    bool XvectorReady() const;

    /**
        This function, which must only be called if XvectorReady() has
        just returned true,  outputs an xvector for an utterance.
            @param [out] utt  The utterance-id is written to here.
                            Note: these will be output in the same order
                            as the user called AcceptUtterance(), except
                            that if opts_.pad_input is false and
                            and utterance is shorter than the chunk
                            size, some utterances may be skipped.
            @param [out] xvector  The xvector will be written to here.
        */
    void OutputXvector(std::string* utt,
        Vector<BaseFloat>* xvector);


    /**
        Calling this will force any partial minibatch to be computed,
        so that any utterances that have previously been passed to
        AcceptUtterance() will, when this function returns, have
        their xvectors ready to be retrieved by OutputXvector().
        */
    void Flush();


private:

    struct XvectorTask {
        std::string utt_id;
        int32 num_chunks;
        int32 num_chunks_finished;
        Vector<BaseFloat> xvector;
        XvectorTask* tail;
    };


    /**
        This decides how to split the utterance into chunks.  It does so in a way
        that minimizes the variance of the x-vector under some simplifying
        assumptions.  It's about minimizing the variance of the x-vector.  We treat
        the x-vector as computed as a sum over frames (although some frames may be
        repeated or omitted due to gaps between chunks or overlaps between chunks);
        and we try to minimize the variance of the x-vector estimate; this is minimized
        when all the frames have the same weight, which is only possible if it can be
        exactly divided into chunks; anyway, this function computes the best division
        into chunks.

        It's a question of whether to allow overlaps or gaps.
        Suppose we are averaging independent quantities with variance 1.  The
        variance of a simple sum of M of those quantities is 1/M.
        Suppose we have M of those quantities, plus N which are repeated twice
        in the sum.  The variance of the estimate formed that way is:

        (M + 4N) / (M + 2N)^2

        If we can't divide it exactly into chunks we'll compare the variances from
        the cases where there is a gap vs. an overlap, and choose the one with
        the smallest variance.  (Note: due to context effects we actually lose
        total_context_ frames from the input signal, and the chunks would have
        to overlap by total_context_ even if the part at the statistics-computation
        layer were ideally cut up.

            @param [in] num_frames  The number of frames in the utterance
            @param [out] start_frames  This function will output to here a vector
                        containing all the start-frames of chunks in this utterance.
                        All chunks will have duration opts_.chunk_size; if a chunk
                        goes past the end of the input we'll repeat the last frame.
                        (This will only happen if opts_.pad_input is false and
                        num_frames is less than opts_.chunk_length.)
        */
    void SplitUtteranceIntoChunks(int32 num_frames,
        std::vector<int32>* start_frames);

    /** This adds a newly created XvectorTask at the tail of the singly linked
        list whose (head,tail) are results_head_, results_tail_.
        */
    XvectorTask* CreateTask(const std::string& utt, int32 num_chunks);


    /**
        Does the nnet computation for one batch and distributes the
        computed x-vectors (of chunks) appropriately to their XvectorTask
        objects.
        */
    void ComputeOneBatch();

    /**
        Adds a new chunk to a batch we are preparing.  This will go
        at position `position_in_batch_` which will be incremented.
            @param [in] task  The task this is part of (records the
                    utterance); tasks_this_batch_[position_in_batch_] will
                    be set to this.
            @param [in] input  The input matrix of features of
                    which this chunk is a part
            @param [in] chunk_start  The frame at which this
                    chunk starts.  Must be >= 0; and if
                    opts_.pad_input is false, chunk_start + opts_.chunk_size
                    must be <= input.NumRows().
        */
    void AddChunkToBatch(XvectorTask* task,
        const Matrix<BaseFloat>& input,
        int32 chunk_start);

    const BatchedXvectorComputerOptions& opts_;
    int32 total_context_;
    const Nnet& nnet_;

    int32 feature_dim_;
    int32 xvector_dim_;

    /**
        Staging area for the input features prior to copying them to GPU.
        Dimension is opts_.chunk_size * opts_.batch_size by feature_dim_.  The
        sequences are interleaved (will be faster since this corresponds to how
        nnet3 keeps things in memory), i.e. row 0 of input_feats_ is time t=0
        for chunk n=0; and row 1 of input_feats_ is time t=0 for chunk n=1.
    */
    Matrix<BaseFloat> input_feats_;


    /** The compiled computation (will be the same for every batch).  */
    std::shared_ptr<const NnetComputation> computation_;


    /**  position_in_batch_ is the number of chunks that we have filled in in
            the input_feats_ matrix and tasks_this_batch_.  When it reaches
            opts_.batch_size we will do the actual computation.
    */
    int32 position_in_batch_;

    /**
        tasks_this_batch_ is of dimension opts_.batch_size.  It is a vector of pointers to
        elements of the singly linked list whose head is at results_head_, or
        NULL for elements with indexes >= position_in_batch_.
        */
    std::vector<XvectorTask*> tasks_this_batch_;

    // results_head_ is the first element in the singly linked list of
    // already-computed xvectors, or NULL if that list is empty.  Note:
    // utterances that are ready will appear here first; new utterances
    // get added to the tail.
    XvectorTask* results_head_;
    // results_tail_ is the last element in the singly linked list of
    // already-computed xvectors, or NULL if the list is empty.
    XvectorTask* results_tail_;
};

BatchedXvectorComputer::XvectorTask*
    BatchedXvectorComputer::CreateTask(
        const std::string& utt, int32 num_chunks) {
    XvectorTask* task = new XvectorTask;
    task->utt_id = utt;
    task->num_chunks = num_chunks;
    task->num_chunks_finished = 0;
    task->xvector.Resize(xvector_dim_);
    task->tail = NULL;
    if (results_tail_) {
        results_tail_->tail = task;
        results_tail_ = task;
    }
    else {  // List was previously empty.
        results_head_ = task;
        results_tail_ = task;
    }
    return task;
}

BatchedXvectorComputer::BatchedXvectorComputer(
    const BatchedXvectorComputerOptions& opts,
    const Nnet& nnet,
    int32 total_context) :
    opts_(opts),
    total_context_(total_context),
    nnet_(nnet),
    position_in_batch_(0),
    results_head_(NULL),
    results_tail_(NULL) {

    tasks_this_batch_.resize(opts_.batch_size);

    feature_dim_ = nnet.InputDim("input");
    xvector_dim_ = nnet.OutputDim("output");
    // Zero input_feats_ in case there is only one batch, to avoid
    // NaN's being generated due to undefined data.
    input_feats_.Resize(opts_.chunk_size * opts_.batch_size,
        feature_dim_);

    CachingOptimizingCompiler compiler(nnet, opts.optimize_config,
        opts.compiler_config);

    {  // This block creates computation_.
        ComputationRequest request;
        request.need_model_derivative = false;
        request.store_component_stats = false;
        request.inputs.resize(1);
        IoSpecification& input(request.inputs[0]);
        input.name = "input";
        input.has_deriv = false;
        input.indexes.resize(opts_.batch_size * opts_.chunk_size);
        // Note: the sequences are interleaved in the input; this will save an extra
        // copy since it corresponds to how nnet3 stores things by default.  (Makes
        // TDNNs easier to implement.)
        for (int32 n = 0; n < opts_.batch_size; n++) {
            for (int32 t = 0; t < opts_.chunk_size; t++) {
                Index index;
                index.n = n;
                index.t = t;
                // index.x is 0 by default.
                input.indexes[n + opts_.batch_size * t] = index;
            }
        }
        IoSpecification output;
        output.name = "output";
        output.has_deriv = false;
        output.indexes.resize(opts_.batch_size);
        for (int32 n = 0; n < opts_.batch_size; n++) {
            Index index;
            index.n = n;
            index.t = 0;
            output.indexes[n] = index;
        }
        request.outputs.push_back(output);
        computation_ = compiler.Compile(request);
    }
}

void BatchedXvectorComputer::AddChunkToBatch(
    XvectorTask* task,
    const Matrix<BaseFloat>& input,
    int32 chunk_start) {
    int32 n = position_in_batch_++;
    KALDI_ASSERT(n >= 0 && n < opts_.batch_size);
    tasks_this_batch_[n] = task;
    int32 T = opts_.chunk_size,
        num_input_frames = input.NumRows();
    KALDI_ASSERT(input_feats_.NumRows() == T * opts_.batch_size);
    if (input.NumCols() != feature_dim_) {
        KALDI_ERR << "Feature dimension mismatch: neural net expected "
            << feature_dim_ << ", got " << input.NumCols();
    }
    for (int32 t = 0; t < T; t++) {
        SubVector<BaseFloat> dest(input_feats_, t * opts_.batch_size + n);
        int32 src_t = t + chunk_start;
        if (src_t >= num_input_frames) {
            KALDI_ASSERT(opts_.pad_input);
            src_t = num_input_frames - 1;  // Pad with repeats of the last frame.
        }
        SubVector<BaseFloat> src(input, src_t);
        dest.CopyFromVec(src);
    }
}

bool BatchedXvectorComputer::XvectorReady() const {
    if (results_head_ == NULL)
        return false;
    KALDI_ASSERT(results_head_->num_chunks_finished <= results_head_->num_chunks);
    return results_head_->num_chunks_finished == results_head_->num_chunks;
}

void BatchedXvectorComputer::OutputXvector(std::string* utt,
    Vector<BaseFloat>* xvector) {
    KALDI_ASSERT(XvectorReady());
    *utt = results_head_->utt_id;
    xvector->Swap(&(results_head_->xvector));
    XvectorTask* new_tail = results_head_->tail;
    delete results_head_;
    results_head_ = new_tail;
    if (new_tail == NULL)
        results_tail_ = NULL;
}

void BatchedXvectorComputer::Flush() {
    if (position_in_batch_ == 0)
        return;
    ComputeOneBatch();
}


void BatchedXvectorComputer::ComputeOneBatch() {

    CuMatrix<BaseFloat> cu_input_feats(input_feats_);
    Nnet* nnet_to_update = NULL;  // we're not doing any update.
    NnetComputer computer(opts_.compute_config, *computation_,
        nnet_, nnet_to_update);
    computer.AcceptInput("input", &cu_input_feats);
    computer.Run();
    CuMatrix<BaseFloat> cu_output;
    computer.GetOutputDestructive("output", &cu_output);
    KALDI_ASSERT(cu_output.NumRows() == opts_.batch_size);
    Matrix<BaseFloat> output(cu_output);
    for (int32 n = 0; n < opts_.batch_size; n++) {
        XvectorTask* task = tasks_this_batch_[n];
        if (task == NULL)
            continue;  // Would only happen for the last batch.
        task->num_chunks_finished++;
        task->xvector.AddVec(1.0 / task->num_chunks, output.Row(n));
    }
    position_in_batch_ = 0;
    std::fill(tasks_this_batch_.begin(), tasks_this_batch_.end(),
        (XvectorTask*)NULL);
}

void BatchedXvectorComputer::AcceptUtterance(
    const std::string& utt,
    const Matrix<BaseFloat>& input) {
    std::vector<int32> chunk_starts;
    int32 num_frames = input.NumRows();
    SplitUtteranceIntoChunks(num_frames, &chunk_starts);
    int32 num_chunks = chunk_starts.size();
    XvectorTask* task = CreateTask(utt, num_chunks);

    for (int32 i = 0; i < num_chunks; i++) {
        AddChunkToBatch(task, input, chunk_starts[i]);
        if (position_in_batch_ == opts_.batch_size) {
            ComputeOneBatch();
        }
    }
}

void BatchedXvectorComputer::SplitUtteranceIntoChunks(
    int32 num_frames, std::vector<int32>* start_frames) {
    start_frames->clear();
    if (num_frames <= opts_.chunk_size) {
        if (num_frames == opts_.chunk_size || opts_.pad_input)
            start_frames->push_back(0);
        // if we leave start_frames empty, then we just won't compute anything for
        // this file.
    }
    else {
        // these modified quantities are to account for the context effects...  when
        // the chunks overlap by exactly total_context_, the frames that get
        // averaged by the respective chunks in their averaging layers would touch
        // but not overlap.  So the optimal separation between chunks would equal
        // opts_.chunk_size - total_context_.
        int32 modified_num_frames = num_frames - total_context_,
            modified_chunk_size = opts_.chunk_size - total_context_;
        KALDI_ASSERT(modified_num_frames > modified_chunk_size);
        int32 num_chunks1 = modified_num_frames / modified_chunk_size,
            num_chunks2 = num_chunks1 + 1;
        int32 num_frames1 = num_chunks1 * modified_chunk_size,
            num_frames2 = num_chunks2 * modified_chunk_size;
        KALDI_ASSERT(num_frames2 > modified_chunk_size);
        // The M and N below correspond to the M and N in the comment:
        // M is the number of frames repeated once in the averaging, N
        // the number of frames repeated twice.  (Basically a solution
        // of the equations: (M + 2N == num_frames2, M+N == modified_num_frames).
        // Note: by a "frame" above, I mean a specific "t" value in
        // the utterance.
        int32 N = num_frames2 - modified_num_frames,
            M = modified_num_frames - N;
        KALDI_ASSERT(M + 2 * N == num_frames2 && M + N == modified_num_frames);

        // The variances below are proportional to the variance of our
        // estimate of the xvector under certain simplifying assumptions..
        // they help us choose whether to have gaps between the chunks
        // or overlaps between them.
        BaseFloat variance1 = 1.0 / num_frames1,  // the 1/M mentioned above.
            variance2 = (M + 4.0 * N) / ((M + 2.0 * N) * (M + 2.0 * N));
        if (variance1 <= variance2) {
            // We'll choose the smaller number of chunks.  There may be gaps.
            // Counting the positions at the ends, there are num_chunks+1 positions
            // where there might be gaps.
            // Note: "total_gap" is >= 0, it's the positive of the sum of the
            // sizes of those gaps.
            int32 num_chunks = num_chunks1,
                num_gaps = num_chunks + 1,
                total_gap = modified_num_frames - num_chunks * modified_chunk_size;
            KALDI_ASSERT(0 <= total_gap && total_gap < modified_chunk_size);
            std::vector<int32> gap_sizes;  // elements will be >= 0.
            DivideIntoPieces(total_gap, num_gaps, &gap_sizes);
            int32 pos = gap_sizes[0];
            for (int32 i = 0; i < num_chunks; i++) {
                start_frames->push_back(pos);
                pos += modified_chunk_size + gap_sizes[i + 1];
            }
            KALDI_ASSERT(pos == modified_num_frames);
        }
        else {
            int32 num_chunks = num_chunks2,
                num_overlaps = num_chunks - 1,
                total_overlap = modified_num_frames - num_chunks * modified_chunk_size;
            KALDI_ASSERT(-modified_chunk_size < total_overlap&& total_overlap <= 0);
            std::vector<int32> overlap_sizes;  // elements will be <= 0.
            DivideIntoPieces(total_overlap, num_overlaps, &overlap_sizes);
            int32 pos = 0;
            for (int32 i = 0; i < num_chunks; i++) {
                start_frames->push_back(pos);
                pos += modified_chunk_size;
                if (i < num_overlaps)
                    pos += overlap_sizes[i];
            }
            KALDI_ASSERT(pos == modified_num_frames);
        }
    }
}

std::vector<Vector<BaseFloat>> xvector_computation(BatchedXvectorComputer xvector_computer,
    const Matrix<BaseFloat>& input_features,
    const std::vector<std::tuple<std::string, int32, int32, BaseFloat>>& wordsequence){
    std::vector<Vector<BaseFloat>> output_xvector;
    // Sliding Winodw for CMVN compensation
    SlidingWindowCmnOptions cmvn_opts_;
    cmvn_opts_.center = true;
    cmvn_opts_.normalize_variance = false;
    cmvn_opts_.cmn_window = 300;

    Matrix<BaseFloat> full_features(input_features.NumRows(), input_features.NumCols(), kUndefined);
    SlidingWindowCmn(cmvn_opts_, input_features, &full_features);
    KALDI_ASSERT(full_features.NumRows() > 0);
    int32 frame_count = 0;
    output_xvector.resize(wordsequence.size());

    for (int i = 0; i < wordsequence.size(); i++) {
        std::string utt = std::to_string(i);
        auto word = wordsequence[i];
        std::string word_text = std::get<0>(word);
        int32 word_start = std::get<1>(word);
        int32 word_end = std::get<2>(word);
        BaseFloat confidence = std::get<3>(word);
        Matrix<BaseFloat> features;
        features.Resize(word_end - word_start + 1, full_features.NumCols());
        for (int j = word_start, k = 0; j <= word_end; j++, k++)
            features.Row(k).CopyFromVec(full_features.Row(j));

        if (features.NumRows() == 0) {
            KALDI_WARN << "Zero-length utterance: " << utt;
            continue;
        }
        frame_count += features.NumRows();
        xvector_computer.AcceptUtterance(utt, features);
        while (xvector_computer.XvectorReady()) {
            std::string utt;
            Vector<BaseFloat> xvector;
            xvector_computer.OutputXvector(&utt, &xvector);
            int j; std::istringstream(utt) >> j;
            output_xvector.at(j) = xvector;
        }
    }

    xvector_computer.Flush();
    while (xvector_computer.XvectorReady()) {
        std::string utt;
        Vector<BaseFloat> xvector;
        xvector_computer.OutputXvector(&utt, &xvector);
        int j; std::istringstream(utt) >> j;
        output_xvector.at(j) = xvector;
    }
    return output_xvector;
}

bool EstPca(const Matrix<BaseFloat>& ivector_mat, BaseFloat target_energy,
    const std::string& reco, Matrix<BaseFloat>* mat) {

    // If the target_energy is 1.0, it's equivalent to not applying the
    // conversation-dependent PCA at all, so it's better to exit this
    // function before doing any computation.
    if (ApproxEqual(target_energy, 1.0, 0.001))
        return false;

    int32 num_rows = ivector_mat.NumRows(),
        num_cols = ivector_mat.NumCols();
    Vector<BaseFloat> sum;
    SpMatrix<BaseFloat> sumsq;
    sum.Resize(num_cols);
    sumsq.Resize(num_cols);
    sum.AddRowSumMat(1.0, ivector_mat);
    sumsq.AddMat2(1.0, ivector_mat, kTrans, 1.0);
    sum.Scale(1.0 / num_rows);
    sumsq.Scale(1.0 / num_rows);
    sumsq.AddVec2(-1.0, sum); // now sumsq is centered covariance.
    int32 full_dim = sum.Dim();

    Matrix<BaseFloat> P(full_dim, full_dim);
    Vector<BaseFloat> s(full_dim);

    try {
        if (num_rows > num_cols)
            sumsq.Eig(&s, &P);
        else
            Matrix<BaseFloat>(sumsq).Svd(&s, &P, NULL);
    }
    catch (...) {
        KALDI_WARN << "Unable to compute conversation dependent PCA for"
            << " recording " << reco << ".";
        return false;
    }

    SortSvd(&s, &P);

    Matrix<BaseFloat> transform(P, kTrans); // Transpose of P.  This is what
                                         // appears in the transform.

    // We want the PCA transform to retain target_energy amount of the total
    // energy.
    BaseFloat total_energy = s.Sum();
    BaseFloat energy = 0.0;
    int32 dim = 1;
    while (energy / total_energy <= target_energy) {
        energy += s(dim - 1);
        dim++;
    }
    Matrix<BaseFloat> transform_float(transform);
    mat->Resize(transform.NumCols(), transform.NumRows());
    mat->CopyFromMat(transform);
    mat->Resize(dim, transform_float.NumCols(), kCopyData);
    return true;
}

// Transforms i-vectors using the PLDA model.
void TransformIvectors(const Matrix<BaseFloat>& ivectors_in,
    const PldaConfig& plda_config, const Plda& plda,
    Matrix<BaseFloat>* ivectors_out) {
    int32 dim = plda.Dim();
    ivectors_out->Resize(ivectors_in.NumRows(), dim);
    for (int32 i = 0; i < ivectors_in.NumRows(); i++) {
        Vector<BaseFloat> transformed_ivector(dim);
        plda.TransformIvector(plda_config, ivectors_in.Row(i), 1.0,
            &transformed_ivector);
        ivectors_out->Row(i).CopyFromVec(transformed_ivector);
    }
}

// Transform the i-vectors using the recording-dependent PCA matrix.
void ApplyPca(const Matrix<BaseFloat>& ivectors_in,
    const Matrix<BaseFloat>& pca_mat, Matrix<BaseFloat>* ivectors_out) {
    int32 transform_cols = pca_mat.NumCols(),
        transform_rows = pca_mat.NumRows(),
        feat_dim = ivectors_in.NumCols();
    ivectors_out->Resize(ivectors_in.NumRows(), transform_rows);
    KALDI_ASSERT(transform_cols == feat_dim);
    ivectors_out->AddMatMat(1.0, ivectors_in, kNoTrans,
        pca_mat, kTrans, 0.0);
}


int VBDiarization( BatchedXvectorComputerOptions opts, 
                    const Vector<BaseFloat>& xvector_mean,
                    const Matrix<BaseFloat>& xvector_transform,
                    const Plda& plda,
                    std::vector<Vector<BaseFloat>>& xvectors,
                    std::vector<int32>& spk_ids){

    //
    // ivector-subtract-global-mean $pldadir/mean.vec: global mean suntraction 
    //
    int num_speakers = 0;
    int32 size = xvectors.size();
    if (size == 0) {

        //KALDI_WARN << "Not producing output for recording "
        //           << " since no segments had XVectors";
        return num_speakers;
    }




    //
    // Step 1: Mean Normalization of X-vectors
    //
    
    // when there is a global mean
    for (int32 i = 0; i< size; i++ ) 
        xvectors[i].AddVec(-1.0, xvector_mean);

    // subtraction in domain global mean 
    Vector<double> sum;
    int64 num_done = 0;

    for (size_t i = 0; i < size; i++) {
        if (sum.Dim() == 0)
            sum.Resize(xvectors[i].Dim());
        sum.AddVec(1.0, xvectors[i]);
        num_done++;
    }


    //KALDI_LOG << "Read " << num_done << " XVectors.";

    if (num_done != 0) {
        //KALDI_LOG << "Norm of XVector mean was " << (sum.Norm(2.0) / num_done);
        for (size_t i = 0; i < size; i++) {
            xvectors[i].AddVec(-1.0 / num_done, sum);
        }
    }


    //
    //  Step 2: Linear Transform for PCA analysis
    //
    // transform-vec $pldadir/transform.mat: PCA analysis 
    //
    Matrix<BaseFloat> transform(xvector_transform);
    int32 transform_rows = transform.NumRows(),
        transform_cols = transform.NumCols(),
        vec_dim;


    for (int32 i = 0; i < size; i++) {
        vec_dim = xvectors[i].Dim();
        Vector<BaseFloat> vec_out(transform_rows);
        if (transform_cols == vec_dim) {
            vec_out.AddMatVec(1.0, transform, kNoTrans, xvectors[i], 0.0);
        }
        else {
            if (transform_cols != vec_dim + 1) {
                KALDI_ERR << "Dimension mismatch: input vector has dimension "
                    << vec_dim << " and transform has " << transform_cols
                    << " columns.";
            }
            vec_out.CopyColFromMat(transform, vec_dim);
            vec_out.AddMatVec(1.0, transform.Range(0, transform.NumRows(),
                0, vec_dim), kNoTrans, xvectors[i], 1.0);
        }
        xvectors[i].Swap(&vec_out);
    }



    // Step3: Score PLDA : ivector-plda-scoring-dense

    //
    //    target_energy=0.1
    //
    BaseFloat target_energy = 0.1;
    PldaConfig plda_config;
    KALDI_ASSERT(target_energy <= 1.0);

    Plda this_plda(plda);

    Matrix<BaseFloat> ivector_mat(xvectors.size(), xvectors[0].Dim()),
        ivector_mat_pca,
        ivector_mat_plda,
        pca_transform,
        scores(xvectors.size(), xvectors.size());



    for (size_t i = 0; i < xvectors.size(); i++) {
        ivector_mat.Row(i).CopyFromVec(xvectors[i]);
    }


    if (EstPca(ivector_mat, target_energy, "this audio", &pca_transform)) {
        // Apply the PCA transform to the raw i-vectors.
        ApplyPca(ivector_mat, pca_transform, &ivector_mat_pca);

        // Apply the PCA transform to the parameters of the PLDA model.
        this_plda.ApplyTransform(Matrix<double>(pca_transform));

        // Now transform the i-vectors using the reduced PLDA model.
        TransformIvectors(ivector_mat_pca, plda_config, this_plda,
            &ivector_mat_plda);
    }
    else {
        // If EstPca returns false, we won't apply any PCA.
        TransformIvectors(ivector_mat, plda_config, this_plda,
            &ivector_mat_plda);
    }
    for (int32 i = 0; i < ivector_mat_plda.NumRows(); i++) {
        for (int32 j = 0; j < ivector_mat_plda.NumRows(); j++) {
            scores(i, j) = this_plda.LogLikelihoodRatio(Vector<double>(
                ivector_mat_plda.Row(i)), 1.0,
                Vector<double>(ivector_mat_plda.Row(j)));
        }
    }


    //
    // Step 4 : Main loop of AHC
    // agglomerative-cluster
    //


    //BaseFloat threshold = -0.5, 
    BaseFloat threshold = -0.3,
        max_spk_fraction = 1.0;
    bool read_costs = false;
    int32 first_pass_max_utterances = std::numeric_limits<int16>::max();

    if (!read_costs)
        threshold = -threshold;


    // By default, the scores give the similarity between pairs of
    // utterances.  We need to multiply the scores by -1 to reinterpet
    // them as costs (unless --read-costs=true) as the agglomerative
    // clustering code requires.
    if (!read_costs)
        scores.Scale(-1);
    spk_ids.clear();
    if (opts.num_speakers > 0) {
        if (1.0 / opts.num_speakers <= max_spk_fraction && max_spk_fraction <= 1.0)
            AgglomerativeCluster(scores, std::numeric_limits<BaseFloat>::max(),
                opts.num_speakers, first_pass_max_utterances,
                max_spk_fraction, &spk_ids);
        else
            AgglomerativeCluster(scores, std::numeric_limits<BaseFloat>::max(),
                opts.num_speakers, first_pass_max_utterances,
                1.0, &spk_ids);
    }
    else
    {
        AgglomerativeCluster(scores, threshold, 1, first_pass_max_utterances,
            1.0, &spk_ids);
    }

    //
    // Step 5: Post processing of speaker id sequences
    //
    num_speakers = 0;
    for (int32 i = 0; i < spk_ids.size(); i++) {
        int32 label = spk_ids[i];
        if (num_speakers < label)
            num_speakers = label;
    }



    // cout << "Number of speakers =" << num_speakers << std::endl;  

    //
    // Speaker Label Updates for lower number early arrived at
    //

    int32 temp_number = num_speakers + 1;
    int32 start_pos = 0;
    for (int32 spk = 1; spk <= num_speakers; spk++) {
        int32 label = -1;
        for (int32 i = start_pos; i < spk_ids.size(); i++) {
            if (spk == spk_ids[i]) { start_pos++; break; }
            if (spk < spk_ids[i]) {
                label = spk_ids[i];
                break;
            }
        }
        if (label > spk) // update
        {
            for (int32 i = 0; i < spk_ids.size(); i++) {
                if (spk_ids[i] == label)
                    spk_ids[i] = temp_number;

            }
            for (int32 i = 0; i < spk_ids.size(); i++) {
                if (spk_ids[i] == spk)
                    spk_ids[i] = label;
            }
            for (int32 i = 0; i < spk_ids.size(); i++) {
                if (spk_ids[i] == temp_number)
                    spk_ids[i] = spk;
            }
        }
    }

    /*
    for (int32 i = 0; i < spk_ids_.size(); i++)
         cout << spk_ids_[i] << std::endl;
    */

    //
    // Main loop of VB diarization
    //
    return num_speakers;

}

std::string MakeFinalJSON(  bool has_xvector, 
                            const std::vector<int32>& spk_ids, 
                            const int num_speakers, 
                            const std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence){
    
    json::JSON finalobj;
    bool isNull = true;
    
    if (has_xvector)
    {


        int32 prev_id = -1;
        int32 seg_index = 0;
        std::vector<BaseFloat> speaker_start;
        std::vector<int32> speaker_id;
        for (auto spk : spk_ids) {
            auto word = word_sequence[seg_index++];
            int32 word_start = std::get<1>(word); // in frame
            if (spk != prev_id) { //speaker change point
                speaker_start.push_back(word_start * 0.01 - 0.03);// consideration mismatch between STT and SD frames
                speaker_id.push_back(spk);
                prev_id = spk;
            }
        }
        // last time
        speaker_start.push_back(std::numeric_limits<int32>::max());

        finalobj["id"] = "Revcord";
        finalobj["speakers"] = num_speakers;
        finalobj["callid"] = "555d150000d000801290002918a10000";
        finalobj["confidence"] = 0.995;

        std::stringstream fulltext;
        int32 size = speaker_start.size();

        for (int i = 0; i < size - 1; i++) {
            BaseFloat seg_start = speaker_start[i];
            BaseFloat seg_end = speaker_start[i + 1];
            
            json::JSON obj;
            std::stringstream who;
            who << "speaker" << speaker_id[i];
            obj["who"] = who.str();


            BaseFloat speaker_start_time = -1.0;
            BaseFloat speaker_end_time = -1.0;
            std::stringstream text;
            int32 word_count = 0;
            for (int32 word_index = 0; word_index < word_sequence.size(); word_index++)
            {
                auto word = word_sequence[word_index];
                std::string word_text = std::get<0>(word);
                BaseFloat word_start  = std::get<1>(word) * 0.03; // in seconds
                BaseFloat word_end    = std::get<2>(word) * 0.03;  // in frame offset
                BaseFloat confidence  = std::get<3>(word);
                
                // if ((word_start >= seg_start && word_start <= seg_end) ||
                //     (word_start < seg_start && word_end >= seg_start)) {// compare start time
                if (word_end >= seg_start && word_end < seg_end) {

                    //add word to this speaker group

                   
                    json::JSON word;
                    word["word"] = word_text;
                    word["start"] = word_start;
                    word["end"] = word_end;
                    word["conf"] = confidence;

                    obj["words"].append(word);

                    if (word_count) {
                        text << " ";
                        speaker_end_time = word_end;

                    }
                    else { // word_count = 0

                        speaker_end_time = word_end;
                        speaker_start_time = word_start;
                    }

                    text << word_text;
                    word_count++;
                }
            }

            if (word_count > 0)
            {
                fulltext << text.str() << " ";
                obj["what"] = text.str();
                json::JSON speaker_segment;
                std::stringstream time_start;
                time_start << speaker_start_time;
                speaker_segment["start"] = time_start.str();
                std::stringstream time_end;
                time_end << speaker_end_time;
                speaker_segment["end"] = time_end.str();
                obj["when"] = speaker_segment;
                finalobj["speech2text"].append(obj);
                isNull = false;
            }
            else {
                // KALDI_LOG << "Too short to align to this speaker ";
            }

        }
        finalobj["fulltranscript"] = fulltext.str();
    }

    if (isNull)
    {
        // null object to make postprocessing happy
        finalobj["fulltranscript"] = "";

        json::JSON obj;
        obj["what"] = "";
        json::JSON speaker_segment;
        speaker_segment["start"] = "0.0";
        speaker_segment["end"] = "0.0";

        obj["when"] = speaker_segment;

        json::JSON word;
        word["conf"] = 0.0;
        word["start"] = 0.0;
        word["end"] = 0.0;
        word["word"] = "";

        obj["words"].append(word);

        finalobj["speech2text"].append(obj);
        return finalobj.dump();
    }
    return finalobj.dump();
}

std::string MakeFinalJSONOnlyForSTT(const std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence) {

    json::JSON finalobj;
    finalobj["id"] = "Revcord";
    finalobj["speakers"] = 1;
    finalobj["callid"] = "555d150000d000801290002918a10000";
    finalobj["confidence"] = 0.995;

    std::stringstream fulltext;

    json::JSON obj;
    std::stringstream who;
    who << "speaker" << 1;
    obj["who"] = who.str();


    BaseFloat speaker_start_time = -1.0;
    BaseFloat speaker_end_time = -1.0;
    std::stringstream text;
    int32 word_count = 0;
    for (int32 word_index = 0; word_index < word_sequence.size(); word_index++)
    {
        auto it_word = word_sequence[word_index];
        std::string word_text = std::get<0>(it_word);
        BaseFloat word_start = std::get<1>(it_word) * 0.03; // in seconds
        BaseFloat word_end = std::get<2>(it_word) * 0.03;  // in frame offset
        BaseFloat confidence = std::get<3>(it_word);

        json::JSON word;
        word["word"] = word_text;
        word["start"] = word_start;
        word["end"] = word_end;
        word["conf"] = confidence;

        obj["words"].append(word);

        if (word_count) {
            text << " ";
            speaker_end_time = word_end;

        }
        else { // word_count = 0

            speaker_end_time = word_end;
            speaker_start_time = word_start;
        }

        text << word_text;
        word_count++;
    }

    if (word_count > 0)
    {
        fulltext << text.str() << " ";
        obj["what"] = text.str();
        json::JSON speaker_segment;
        std::stringstream time_start;
        time_start << speaker_start_time;
        speaker_segment["start"] = time_start.str();
        std::stringstream time_end;
        time_end << speaker_end_time;
        speaker_segment["end"] = time_end.str();
        obj["when"] = speaker_segment;
        finalobj["speech2text"].append(obj);
    }
    else {
        // KALDI_LOG << "Too short to align to this speaker ";
    }

    
    finalobj["fulltranscript"] = fulltext.str();
   
    return finalobj.dump();
}


} // namespace cuda_decoder
}  // namespace kaldi

#endif