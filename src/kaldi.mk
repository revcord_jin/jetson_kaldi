# This file was generated using the following command:
# ./configure --mathlib=OPENBLAS --openblas-root=/usr --use-cuda --cudatk-dir=/usr/local/cuda/ --cuda-arch=-arch=sm_53 --shared

CONFIGURE_VERSION := 12

# Toolchain configuration

CXX = g++
AR = ar
AS = as
RANLIB = ranlib

# Base configuration

KALDI_FLAVOR := dynamic
KALDILIBDIR := /opt/kaldi/src/lib
DEBUG_LEVEL = 1
DOUBLE_PRECISION = 0
OPENFSTINC = /opt/kaldi/tools/openfst-1.7.2/include
OPENFSTLIBS = /opt/kaldi/tools/openfst-1.7.2/lib/libfst.so
OPENFSTLDFLAGS = -Wl,-rpath=/opt/kaldi/tools/openfst-1.7.2/lib

CUBROOT = /opt/kaldi/tools/cub-1.8.0
WITH_CUDADECODER = 1

OPENBLASINC = /usr/include/aarch64-linux-gnu
OPENBLASLIBS = -L/usr/lib/aarch64-linux-gnu -lopenblas -lgfortran -Wl,-rpath=/usr/lib/aarch64-linux-gnu

# OpenBLAS specific Linux ARM configuration

ifndef DEBUG_LEVEL
$(error DEBUG_LEVEL not defined.)
endif
ifndef DOUBLE_PRECISION
$(error DOUBLE_PRECISION not defined.)
endif
ifndef OPENFSTINC
$(error OPENFSTINC not defined.)
endif
ifndef OPENFSTLIBS
$(error OPENFSTLIBS not defined.)
endif
ifndef OPENBLASINC
$(error OPENBLASINC not defined.)
endif
ifndef OPENBLASLIBS
$(error OPENBLASLIBS not defined.)
endif

CXXFLAGS = -std=c++11 -I.. -isystem $(OPENFSTINC) -O3 -march=armv8-a+crypto $(EXTRA_CXXFLAGS) \
           -Wall -Wno-sign-compare -Wno-unused-local-typedefs \
           -Wno-deprecated-declarations -Winit-self \
           -DKALDI_DOUBLEPRECISION=$(DOUBLE_PRECISION) \
           -DHAVE_EXECINFO_H=1 -DHAVE_CXXABI_H -DHAVE_OPENBLAS -I$(OPENBLASINC) \
           -ftree-vectorize -pthread \
           -g

ifeq ($(KALDI_FLAVOR), dynamic)
CXXFLAGS += -fPIC
endif

ifeq ($(DEBUG_LEVEL), 0)
CXXFLAGS += -DNDEBUG
endif
ifeq ($(DEBUG_LEVEL), 2)
CXXFLAGS += -O0 -DKALDI_PARANOID
endif

# Compiler specific flags
COMPILER = $(shell $(CXX) -v 2>&1)
ifeq ($(findstring clang,$(COMPILER)),clang)
# Suppress annoying clang warnings that are perfectly valid per spec.
CXXFLAGS += -Wno-mismatched-tags
endif

LDFLAGS = $(EXTRA_LDFLAGS) $(OPENFSTLDFLAGS) -rdynamic
LDLIBS = $(EXTRA_LDLIBS) $(OPENFSTLIBS) $(OPENBLASLIBS) -lm -lpthread -ldl

# CUDA configuration

CUDA = true
CUDATKDIR = /usr/local/cuda-10.2
CUDA_ARCH = -arch=sm_53

ifndef DOUBLE_PRECISION
$(error DOUBLE_PRECISION not defined.)
endif
ifndef CUDATKDIR
$(error CUDATKDIR not defined.)
endif

CXXFLAGS += -DHAVE_CUDA -I$(CUDATKDIR)/include -fPIC -pthread -isystem $(OPENFSTINC)

CUDA_INCLUDE= -I$(CUDATKDIR)/include -I$(CUBROOT) -I.. -isystem $(OPENFSTINC)
CUDA_FLAGS = --compiler-options -fPIC --machine 64 -DHAVE_CUDA \
             -ccbin $(CXX) -DKALDI_DOUBLEPRECISION=$(DOUBLE_PRECISION) \
             -std=c++14 -DCUDA_API_PER_THREAD_DEFAULT_STREAM -lineinfo \
             --verbose -Wno-deprecated-gpu-targets

CUDA_LDFLAGS += -L$(CUDATKDIR)/lib64/stubs -L$(CUDATKDIR)/lib64 -Wl,-rpath,$(CUDATKDIR)/lib64
CUDA_LDFLAGS += -L$(CUDATKDIR)/lib/stubs -L$(CUDATKDIR)/lib -Wl,-rpath,$(CUDATKDIR)/lib

CUDA_LDLIBS += -lcuda -lcublas -lcusparse -lcudart -lcurand -lcufft -lnvToolsExt #LDLIBS : The .so libs are loaded later than static libs in implicit rule
CUDA_LDLIBS += -lcusolver

# Environment configuration

