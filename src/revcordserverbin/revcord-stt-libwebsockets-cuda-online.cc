// cudadecoderbin/batched-wav-nnet3-cuda-online.cc
//
// Copyright (c) 2019, NVIDIA CORPORATION.  All rights reserved.
// Hugo Braun
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//#define lws_verbose


//#define MEMORY_LEAKS_DETECTION

#ifdef MEMORY_LEAKS_DETECTION
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#endif

#if HAVE_CUDA == 1

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <nvToolsExt.h>
#include <iomanip>
#include "cudadecoder/batched-threaded-nnet3-cuda-online-pipeline.h"
#include "cudadecoder/cuda-online-pipeline-dynamic-batcher.h"
#include "cudadecoderbin/cuda-bin-tools.h"
//#include "custom_backend.h"
#include "cudadecoderbin/cuda-speaker-diarization-tools.h"
#include "cudamatrix/cu-allocator.h"
#include "fstext/fstext-lib.h"
#include "lat/lattice-functions.h"
#include "nnet3/am-nnet-simple.h"
#include "nnet3/nnet-utils.h"
#include "util/kaldi-thread.h"
#include "lat/sausages.h"
#include "cudadecoderbin/json.h"

#include <iostream>
#include <istream>
#include <streambuf>
#include <string>


//Web socket-related includes
#define LWS_DLL
#define LWS_INTERNAL
#include "libwebsockets.h"
#include <signal.h>


#include "revcordserver/wavchunk.pb.h"
using namespace revcord;
using namespace std;
using namespace kaldi;
using namespace cuda_decoder;

enum ErrorCodes {
    kSuccess,
    kUnknown,
    kInvalidModelConfig,
    kGpuNotSupported,
    kSequenceBatcher,
    kModelControl,
    kInputOutput,
    kInputName,
    kOutputName,
    kInputOutputDataType,
    kInputContents,
    kInputSize,
    kOutputBuffer,
    kBatchTooBig,
    kTimesteps,
    kChunkTooBig
};

CudaOnlineBinaryOptions opts_;
BatchedThreadedNnet3CudaOnlinePipeline* cuda_pipeline_ = NULL;
CudaOnlinePipelineDynamicBatcher* dynamic_batcher_ = NULL;

fst::SymbolTable* word_syms_;
BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID correlation_id_ = 0;

bool latest_sent_msg_was_buffered = false;
// Reply with ....
unsigned char buf[LWS_SEND_BUFFER_PRE_PADDING + 65536 * 8 + LWS_SEND_BUFFER_POST_PADDING];
unsigned char* writable_buffer_ = &buf[LWS_SEND_BUFFER_PRE_PADDING];
size_t writable_size_;

// Contants 
const uint64_t int32_byte_size_ = 4;
const uint64_t int64_byte_size_ = 8;
const uint64_t chunk_num_bytes_ = 8160;
const uint64_t chunk_num_samps_ = 4080;// 8000 * 0.51

#ifdef XVECTOR_SD
Vector<BaseFloat> xvector_mean_;
Matrix<BaseFloat> xvector_transform_;
Plda plda_;
BatchedXvectorComputer* xvector_computer_;

std::string SpeakerDiarization(const CudaOnlineBinaryOptions opts, fst::SymbolTable& word_syms,
    const kaldi::CompactLattice& clat,
    const Matrix<BaseFloat>& h_utt_features,
    const BatchedXvectorComputer& xvector_computer,
    const Vector<BaseFloat>& xvector_mean,
    const Matrix<BaseFloat>& xvector_transform,
    const Plda& plda) {

    MinimumBayesRisk mbr(clat);
    const std::vector<BaseFloat>& conf = mbr.GetOneBestConfidences();
    const std::vector<int32>& words = mbr.GetOneBest();
    const std::vector<std::pair<BaseFloat, BaseFloat> >& times =
        mbr.GetOneBestTimes();

    int size = words.size();
    // Create JSON object, SAD and word segment
    std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence;
    for (int i = 0; i < size; i++) {
        json::JSON word;
        int32 word_start = times[i].first; // in frame offset
        int32 word_end = times[i].second;  // in frame offset
        word_sequence.push_back(std::make_tuple(word_syms.Find(words[i]),
            word_start, word_end, conf[i]));
    }

    std::vector<Vector<BaseFloat>> xvectors = xvector_computation(xvector_computer, h_utt_features, word_sequence);
    bool has_xvector = (xvectors.size() > 0);
    std::vector<int32> spk_ids;
    int num_speakers = VBDiarization(opts.batched_xvector_opts, xvector_mean, xvector_transform, plda, xvectors, spk_ids);
    std::string out_str = MakeFinalJSON(has_xvector, spk_ids, num_speakers, word_sequence);
    return out_str;
}
#endif

std::string BuildFinalResult(const CudaOnlineBinaryOptions opts, fst::SymbolTable& word_syms,
    const kaldi::CompactLattice& clat) 
{

    MinimumBayesRisk mbr(clat);
    const std::vector<BaseFloat>& conf = mbr.GetOneBestConfidences();
    const std::vector<int32>& words = mbr.GetOneBest();
    const std::vector<std::pair<BaseFloat, BaseFloat> >& times =
        mbr.GetOneBestTimes();

    int size = words.size();
    // Create JSON object, SAD and word segment
    std::vector<std::tuple<std::string, int32, int32, BaseFloat>> word_sequence;
    json::JSON word;
    for (int i = 0; i < size; i++) {
        
        int32 word_start = times[i].first; // in frame offset
        int32 word_end = times[i].second;  // in frame offset
        word_sequence.push_back(std::make_tuple(word_syms.Find(words[i]),
            word_start, word_end, conf[i]));
    }
    std::string out_str = MakeFinalJSONOnlyForSTT(word_sequence);
    return out_str;
}

//*****************************
//*** AUDIO STREAMING LAYER ***
//*****************************

//Server configuration definitions
#define TIME_INTERVAL 50
const int DEFAULT_SERVER_PORT = 2571;
static int interrupted, port = DEFAULT_SERVER_PORT;

// audio stream buffers
const int MAX_NUM_CHANNELS = 1024;

std::recursive_mutex hypotheses_m_;
queue<pair<Info_Kind, string>> hypotheses_[MAX_NUM_CHANNELS];
queue<Info_Kind> write_events_[MAX_NUM_CHANNELS];
bool channel_used[MAX_NUM_CHANNELS];


struct audio_streaming_protocol_instance {
    char buffer[ chunk_num_bytes_ + sizeof(WavChunk)];
    BaseFloat wav[chunk_num_samps_];
    uint64_t total_bytes_received;
    BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID corr_id;
    bool live_stt;
    int n_total_chunks;
    int channel_no;
    queue<pair<Info_Kind, string>> *hypotheses;
    queue<Info_Kind> *write_events;
};



bool WriteSTTChunk(struct lws* wsi, STTChunk chunk)
{
    writable_size_ = chunk.ByteSizeLong();
    chunk.SerializeToArray(writable_buffer_, writable_size_);
    return true;
}
bool Reply(struct lws* wsi, struct audio_streaming_protocol_instance* ldi, Info_Kind info) {

    ldi->write_events->push(info);
    lws_callback_on_writable(wsi);
    return true;
}

int AddResult(struct audio_streaming_protocol_instance* ldi, std::string str, bool partial, bool endpoint_detected, bool final)
{
    pair<Info_Kind, string> pair_result;
    if (partial)
        pair_result = make_pair(Info_Kind::Info_Kind_PARTIALRESULT, str);
    else if (endpoint_detected)
        pair_result = make_pair(Info_Kind::Info_Kind_ENDPOINTRESULT, str);
    else if (final)
        pair_result = make_pair(Info_Kind::Info_Kind_FINALRESULT, str);

    {
        std::lock_guard<std::recursive_mutex> lk(hypotheses_m_);
        ldi->hypotheses->push(pair_result);
    }
    return kSuccess;
}

int ProcessWavChunk(struct lws* wsi, WavChunk chunk, struct audio_streaming_protocol_instance* ldi)
{
    int32_t length = chunk.samples().length() / 2;
    short* wav_short = (short*)(chunk.samples().data());
    for (int i = 0; i < length; i++)
        ldi->wav[i] = (BaseFloat)wav_short[i];
    SubVector<BaseFloat> wave_part(ldi->wav, length);
    bool start = chunk.is_first_chunk();
    bool end = chunk.is_last_chunk();
    BatchedThreadedNnet3CudaOnlinePipeline::CorrelationID corr_id = chunk.corr_id();
    KALDI_ASSERT(corr_id == ldi->corr_id);

    if (start) {
        // Defining the callback for the partial results
        cuda_pipeline_->SetBestPathCallback(
            corr_id,
            [corr_id, wsi, ldi](
                const std::string& str, bool partial, bool endpoint_detected) {
                    AddResult(ldi, str, partial, endpoint_detected, false);
            });
    }
    if (end) {
        // If last chunk, set the callback for that seq
        cuda_pipeline_->SetLatticeCallback(
#ifdef XVECTOR_SD
            corr_id, [corr_id](CompactLattice& clat, Matrix<BaseFloat>& h_utt_features) {
                std::string output = SpeakerDiarization(opts_, *word_syms_, clat, h_utt_features, *xvector_computer_, xvector_mean_, xvector_transform_, plda_);
                //std::string output = "{\"text\": \"\"}";
#else
            corr_id, [corr_id, wsi, ldi](CompactLattice& clat) {
                //std::string str = "{\"text\": \"\"}";
                std::string str = BuildFinalResult(opts_, *word_syms_, clat);
#endif
                AddResult(ldi, str, false, false, true);
            });
        }
    dynamic_batcher_->Push(corr_id, start, end, wave_part);
    ldi->n_total_chunks++;
    return kSuccess;
}

int ReceiveChunk(struct lws* wsi, WavChunk chunk, struct audio_streaming_protocol_instance* ldi) {

    Info_Kind kind = chunk.info().kind();
    switch (kind)
    {
        case Info_Kind::Info_Kind_INITSERVER:
        {
            cuda_pipeline_->FreeChannelMap();
            correlation_id_ = 0;
            // Auxiallary initialize
            for (int i = 0; i < MAX_NUM_CHANNELS; i++)
                channel_used[i] = false;
            auto now = std::chrono::system_clock::now();
            std::time_t end_time = std::chrono::system_clock::to_time_t(now);
            std::cout << std::endl << "Initialized the server " << std::ctime(&end_time) << std::endl;
        }
        break;
        case Info_Kind::Info_Kind_WAITFORCOMPLETION:
        {
            dynamic_batcher_->WaitForCompletion();
            auto now = std::chrono::system_clock::now();
            std::time_t end_time = std::chrono::system_clock::to_time_t(now);
            std::cout << std::endl << "Completated. " << std::ctime(&end_time) << std::endl;
        }
        break;
        case Info_Kind::Info_Kind_LIVESTT:
        {
            ldi->live_stt = true;
        }
        break;
        case Info_Kind::Info_Kind_CLOSE:
        {
        }
        break;
        case Info_Kind::Info_Kind_WAV:
            ProcessWavChunk(wsi, chunk, ldi);
        break;
        default: break;
    }

    Reply(wsi, ldi, kind);
    return kSuccess;
 }



 int SendResponse(struct lws* wsi, struct audio_streaming_protocol_instance* ldi){
     int result = kSuccess;
     
     try 
     {

         if (latest_sent_msg_was_buffered) {
             if (lws_partial_buffered(wsi)) {
                 latest_sent_msg_was_buffered = true;
                 lws_callback_on_writable(wsi);
                 cout << "latest_sent_msg_was_buffered in server BEFORE " << endl;
                 return result;
             }
             else
             {
                 latest_sent_msg_was_buffered = false;
             }
             return result;
         }

         /* but if the peer told us he wants less, we can adapt */
         int m = lws_get_peer_write_allowance(wsi);

         /* -1 means not using a protocol that has this info */
         if (m == 0) {
             /* right now, peer can't handle anything */
             std::cout << "right now, peer can't handle anything " << std::endl;
             return result;
         }

         std::string output = "{\"text\": \"\"}";
         STTChunk chunk;
         if (ldi->write_events->size() > 0)
         {
             Info_Kind kind = ldi->write_events->front();
             ldi->write_events->pop();
             switch (kind)
             {
             case Info_Kind::Info_Kind_INITSERVER:
             case Info_Kind::Info_Kind_WAITFORCOMPLETION:
             case Info_Kind::Info_Kind_LIVESTT:
                    chunk.set_corr_id(-1);// Empty chunk
                    break;
             case Info_Kind::Info_Kind_CLOSE:
                    while (!ldi->hypotheses->empty())
                        ldi->hypotheses->pop();
                    while (!ldi->write_events->empty())
                        ldi->write_events->pop();
                    chunk.set_corr_id(ldi->corr_id);// Empty chunk
                    break;
             case Info_Kind::Info_Kind_GETCORRID:
                    if (correlation_id_ < LLONG_MAX)
                        ldi->corr_id = correlation_id_++;
                    else //re-wrap to zero
                        correlation_id_ = ldi->corr_id = 0;
                    chunk.set_corr_id(ldi->corr_id);
                    ldi->n_total_chunks = 0;
                    break;
             case Info_Kind::Info_Kind_WAV:
                    chunk.set_corr_id(ldi->corr_id);// Empty chunk
                    break;
             case Info_Kind::Info_Kind_GETSTT:
             {
                 chunk.set_corr_id(ldi->corr_id);
                 std::unique_lock<std::recursive_mutex> lk(hypotheses_m_);
                 if (!ldi->hypotheses->empty())
                 {
                     pair<Info_Kind, string> hypo = ldi->hypotheses->front();
                     ldi->hypotheses->pop();
                     lk.unlock();
                     Info_Kind stt_kind = hypo.first;
                     string str = hypo.second;
                     switch (stt_kind)
                     {
                     case Info_Kind::Info_Kind_PARTIALRESULT:
                         chunk.set_is_partial_chunk(true);
                         break;
                     case Info_Kind::Info_Kind_ENDPOINTRESULT:
                         chunk.set_is_result_chunk(true);
                         break;
                     case Info_Kind::Info_Kind_FINALRESULT:
                         chunk.set_is_final_chunk(true);
                         break;
                     default: break;
                     }
                     chunk.set_result(str);
                 }
             }
             break;

             default: break;
             }
             
             WriteSTTChunk(wsi, chunk);

             if (m != -1 && m < writable_size_) {
                 /* he couldn't handle that much */
                 std::cout << "he couldn't handle that much " << writable_size_ / 1024.0 << "(KB)" << std::endl;
                 return result;
             }

             int n = lws_write(wsi, writable_buffer_, writable_size_, LWS_WRITE_BINARY);

             if (lws_partial_buffered(wsi)) {
                 latest_sent_msg_was_buffered = true;
                 cout << "latest sent msg was_buffered in server AFTER " << n / 1024.0 << "(KB)" << "corr_id =" << ldi->corr_id << std::endl;
                 lws_callback_on_writable(wsi);
             }
         }
     }
     catch (const std::exception& e) {
            std::cerr << e.what();
     }
     return result;
 }

static int
callback_audio_streaming(struct lws* wsi,
    enum lws_callback_reasons reason,
    void* user, void* in, size_t len)
{

    struct audio_streaming_protocol_instance* ldi = static_cast<struct audio_streaming_protocol_instance*>(user);

    switch (reason) {
    case LWS_CALLBACK_PROTOCOL_INIT: {
       break;

    }
    case LWS_CALLBACK_PROTOCOL_DESTROY: {
        break;
    }

    case LWS_CALLBACK_ESTABLISHED: {
        //auto now = std::chrono::system_clock::now();
        //std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        //std::cout << "the last connection established at " << std::ctime(&end_time) << std::endl;
        ldi->live_stt = false;
        bool notFound = true;
        for (int i = 0; i < MAX_NUM_CHANNELS; i++)
            if (!channel_used[i])
            {
                ldi->channel_no = i;
                ldi->hypotheses = &hypotheses_[i];
                ldi->write_events = &write_events_[i];
                notFound = false;
                channel_used[i] = true;
                break;
            }
        if (notFound)
        {
            std::cout << "no found empty channel when trying to establish a connection" << std::endl;
            return -1;
        }

        break;
    }

    case LWS_CALLBACK_CLOSED: {
        //lwsl_user("LWS_CALLBACK_CLOSED....\n");
        //auto now = std::chrono::system_clock::now();
        //std::time_t end_time = std::chrono::system_clock::to_time_t(now);
        //std::cout << "the last connection closed at " << std::ctime(&end_time) << std::endl;
        channel_used[ldi->channel_no] = false;
        break;
    }

    case LWS_CALLBACK_RECEIVE:
        try {
            char* buffer = (char*)in;
            std::memcpy(ldi->buffer + ldi->total_bytes_received, buffer, len);
            ldi->total_bytes_received += len;
            if (lws_is_final_fragment(wsi) && !lws_remaining_packet_payload(wsi)) { //EOC event
                WavChunk chunk;
                size_t size = ldi->total_bytes_received;
                ldi->total_bytes_received = 0;
                if(chunk.ParseFromArray(ldi->buffer, size)) // convert to batch message 
                {
                    if (ReceiveChunk(wsi, chunk, ldi) != kSuccess)
                        return -1;
                }
                else
                {
                    std::cout << "Error in parsing the chunk...." << std::endl;
                }
            }
            else
            {
               // std::cout << "fragment...." << std::endl;
            }
        }
        catch (const std::exception& e) {
            std::cerr << e.what();
        }
        break;
    case LWS_CALLBACK_SERVER_WRITEABLE: {
        SendResponse(wsi, ldi);// when received close command
        break;
    }

    default:
        break;
    }
    return 0;
}

static struct lws_protocols protocols[] = {
    // first protocol must always be HTTP handler //
    {
        "audio-streaming-protocol", // protocol name - very important!
        callback_audio_streaming,   // callback
        sizeof(struct audio_streaming_protocol_instance), // per_session_data_size
        0,//65536 * 8, // rx_buffer_size
    0,    // id
    NULL, // user
    0     // tx_packet_size
    },
    {
        NULL, NULL, 0, 0   /* End of list */
    }
};

void sigint_handler(int sig)
{
    interrupted = 1;
}

/***************************
*   Logging
*
***************************/
LWS_VISIBLE LWS_EXTERN void lwsl_emit_stderr(int level,
    const char* line
)
{
    std::cout << "LWS_LOG():" << line << std::endl;
}
//******************
//*** EXECUTABLE ***
//******************


int main(int argc, char* argv[]) {
    try {

#ifdef MEMORY_LEAKS_DETECTION
        _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

        // Verify that the version of the library that we linked against is
        // compatible with the version of the headers we compiled against.
        GOOGLE_PROTOBUF_VERIFY_VERSION;

        // KALDI INITIALIZATION START

        int errno;
        if ((errno = SetUpAndReadCmdLineOptions(argc, argv, &opts_))) return errno;

        TransitionModel trans_model;
        nnet3::AmNnetSimple am_nnet;
        nnet3::Nnet xvector_nnet;
        fst::Fst<fst::StdArc>* decode_fst;
#ifdef XVECTOR_SD 
        ReadModels(opts_, &trans_model, &am_nnet, &decode_fst, &word_syms_,
            &xvector_nnet, &xvector_mean_, &xvector_transform_, &plda_, &xvector_computer_);
#else
        ReadModels(opts_, &trans_model, &am_nnet, &decode_fst, &word_syms_);
#endif
        cuda_pipeline_ = new BatchedThreadedNnet3CudaOnlinePipeline(
            opts_.batched_decoder_config, *decode_fst, am_nnet, trans_model);
        delete decode_fst;
        if (word_syms_) cuda_pipeline_->SetSymbolTable(*word_syms_);

        CudaOnlinePipelineDynamicBatcherConfig dynamic_batcher_config;
        dynamic_batcher_ = new CudaOnlinePipelineDynamicBatcher(dynamic_batcher_config, *cuda_pipeline_);

        nvtxRangePush("Global Timer");

        // KALDI INITIALIZATION FINISHED



        // COMMUNICATION LAYER INITIALIZATION

        struct lws_context_creation_info info;
        struct lws_context* context;
        int n = 0, logs = LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE
            /*LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE*/
            /* for LLL_ verbosity above NOTICE to be built into lws,
            * lws must have been configured and built with
            * -DCMAKE_BUILD_TYPE=DEBUG instead of =RELEASE */
            /* | LLL_INFO */ /* | LLL_PARSER */ /* | LLL_HEADER */
            /* | LLL_EXT */ /* | LLL_CLIENT */ /* | LLL_LATENCY */
            /* | LLL_DEBUG */;

        signal(SIGINT, sigint_handler);
        lws_set_log_level(logs, lwsl_emit_stderr);
        //lws_set_log_level(logs, NULL);

        lwsl_user("\n -------- REVCORD LWS server ----------\n");

        memset(&info, 0, sizeof info); /* otherwise uninitialized garbage */
        info.port = port;
        info.protocols = protocols;
        info.pt_serv_buf_size = 1024 * 128; // 128KB
        info.options = LWS_SERVER_OPTION_VALIDATE_UTF8 |
            LWS_SERVER_OPTION_HTTP_HEADERS_SECURITY_BEST_PRACTICES_ENFORCE;
        
        
        context = lws_create_context(&info);
        if (context == NULL) {
            KALDI_LOG << "libwebsocket init failed";
            return -1;
        }

        auto now = std::chrono::system_clock::now();
        std::time_t end_time = std::chrono::system_clock::to_time_t(now);

        std::cout << std::endl << "Server started at " << std::ctime(&end_time) << std::endl;

        // infinite loop, to end this server send SIGTERM. (CTRL+C)
        while (n >= 0 && !interrupted)
            n = lws_service(context, 0);
        lws_context_destroy(context);
        lwsl_user("Completed %s\n", interrupted == 2 ? "OK" : "failed");

        dynamic_batcher_->WaitForCompletion();

        nvtxRangePop();
        delete word_syms_;  // will delete if non-NULL.
        delete cuda_pipeline_;
        delete dynamic_batcher_;
        cudaDeviceSynchronize();

        // Optional:  Delete all global objects allocated by libprotobuf.
        google::protobuf::ShutdownProtobufLibrary();

#ifdef MEMORY_LEAKS_DETECTION
        _CrtDumpMemoryLeaks();
#endif

        return interrupted != 2;
    }
    catch (const std::exception& e) {
        std::cerr << e.what();
        return -1;
    }
}  // main()

#endif  // if HAVE_CUDA == 1
