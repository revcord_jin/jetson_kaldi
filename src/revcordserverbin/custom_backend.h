#pragma once
#include <stddef.h>
#include <stdint.h>

// The initialization information provided to a custom backend when it
// is created.
typedef struct custom_initdata_struct {
    /// The name of this instance of the custom backend. Instance names
    /// are unique.
    const char* instance_name;

    /// Serialized representation of the model configuration. This
    /// serialization is owned by the caller and must be copied if a
    /// persistent copy of required by the custom backend.
    const char* serialized_model_config;

    /// The size of 'serialized_model_config', in bytes.
    size_t serialized_model_config_size;

    /// The GPU device ID to initialize for, or CUSTOM_NO_GPU_DEVICE if
    /// should initialize for CPU.
    int gpu_device_id;

    /// The number of server parameters (i.e. the length of
    /// 'server_parameters').
    size_t server_parameter_cnt;

    /// The server parameter values as null-terminated strings, indexed
    /// by CustomServerParameter. This strings are owned by the caller
    /// and must be copied if a persistent copy of required by the
    /// custom backend.
    const char** server_parameters;
} CustomInitializeData;
/// A payload represents the input tensors and the required output
/// needed for execution in the backend.
typedef struct custom_payload_struct {
  /// The size of the batch represented by this payload.
  uint32_t batch_size;

  /// The number of inputs included in this payload.
  uint32_t input_cnt;

  /// For each of the 'input_cnt' inputs, the name of the input as a
  /// null-terminated string.
  const char** input_names;

  /// For each of the 'input_cnt' inputs, the number of dimensions in
  /// the input's shape, not including the batch dimension.
  const size_t* input_shape_dim_cnts;

  /// For each of the 'input_cnt' inputs, the shape of the input, not
  /// including the batch dimension.
  const int64_t** input_shape_dims;

  /// The number of outputs that must be computed for this
  /// payload. Can be 0 to indicate that no outputs are required from
  /// the backend.
  uint32_t output_cnt;

  /// For each of the 'output_cnt' outputs, the name of the output as
  /// a null-terminated string.  Each name must be one of the names
  /// from the model configuration, but all outputs do not need to be
  /// computed.
  const char** required_output_names;

  /// The context to use with CustomGetNextInput callback function to
  /// get the input tensor values for this payload.
  void* input_context;

  /// The context to use with CustomGetOutput callback function to get
  /// the buffer for output tensor values for this payload.
  void* output_context;

  /// The error code indicating success or failure from execution. A
  /// value of 0 (zero) indicates success, all other values indicate
  /// failure and are backend defined.
  int error_code;
} CustomPayload;

/// A payload represents the input tensors and the required output
/// needed for execution in the backend.
typedef struct custom_batch_chunk_struct {
    /// The size of the batch represented by this chunk.
    uint32_t payload_cnt;
    /// The chunks included in this batch.
    CustomPayload *payloads;
} BatchChunk;

/// Type for the CustomGetNextInput callback function.
///
/// This callback function is provided in the call to ComputeExecute
/// and is used to get the value of the input tensors. Each call to
/// this function returns a contiguous block of the input tensor
/// value. The entire tensor value may be in multiple non-contiguous
/// blocks and so this function must be called multiple times until
/// 'content' returns nullptr. This callback function is not thread
/// safe.
///
/// \param input_context The input context provided in call to
/// CustomExecute.
/// \param name The name of the input tensor.
/// \param content Returns a pointer to the next contiguous block of
/// content for the named input. Returns nullptr if there is no more
/// content for the input.
/// \param content_byte_size Acts as both input and output. On input
/// gives the maximum size expected for 'content'. Returns the actual
/// size, in bytes, of 'content'.
/// \return false if error, true if success.
typedef bool (*CustomGetNextInputFn_t)(
    void* input_context, const char* name, const void** content,
    uint64_t* content_byte_size);

/// Type for the CustomGetOutput callback function.
///
/// This callback function is provided in the call to ComputeExecute
/// and is used to report the shape of an output and to get the
/// buffers to store the output tensor values. This callback funtion
/// is not thread safe.
///
/// \param output_context The output context provided in call to
/// CustomExecute.
/// \param name The name of the output tensor.
/// \param shape_dim_cnt The number of dimensions in the output shape.
/// \param shape_dims The dimensions of the output shape.
/// \param content_byte_size The size, in bytes, of the output tensor.
/// \param content Returns a pointer to a buffer where the output for
/// the tensor should be copied. If nullptr and function returns true
/// (no error), then the output should not be written and the backend
/// should continue to the next output. If non-nullptr, the size of
/// the buffer will be large enough to hold 'content_byte_size' bytes.
/// \return false if error, true if success.
typedef bool (*CustomGetOutputFn_t)(
    void* output_context, const char* name, size_t shape_dim_cnt,
    int64_t* shape_dims, uint64_t content_byte_size, void** content);

/// Type for the CustomVersion function.
typedef uint32_t (*CustomVersionFn_t)();

/// Type for the CustomInitialize function.
typedef int (*CustomInitializeFn_t)(const CustomInitializeData*, void**);

/// Type for the CustomFinalize function.
typedef int (*CustomFinalizeFn_t)(void*);

/// Type for the CustomErrorString function.
typedef char* (*CustomErrorStringFn_t)(void*, int);

/// Type for the CustomExecute function.
typedef int (*CustomExecuteFn_t)(
    void*, uint32_t, CustomPayload*, CustomGetNextInputFn_t,
    CustomGetOutputFn_t);




