1. Building Kaldi Natively on Jetson 

  # Install missing dependencies
  # L4T image does not contain libopenblas, so use sudo apt-get install libopenblas-dev
  sudo apt-get install -y gfortran swig liblapacke-dev libopenblas-dev nano python3.7 python3-pip
  
  # build openblas, kaldi
  
  git clone https://github.com/kaldi-asr/kaldi.git && cd kaldi/tools/
  sed -i 's:status=0:exit 0:g' extras/check_dependencies.sh
  
  # additional -03 flags were copied from Jetson Nano forums (official answers)
  
  sed -i 's:openfst_add_CXXFLAGS = -g -O2:openfst_add_CXXFLAGS = -g -O3 -march=armv8-a+crypto -mcpu=cortex-a57+crypto:g' Makefile
  sed -i 's:--enable-ngram-fsts:--enable-ngram-fsts --disable-bin:g' Makefile
  
  # automake relevant setting
  export am_api_version='1.15.1'
  
  make -j 1 openfst cub
  
  cd ../src/
  sed -i 's:$OPENBLASROOT/lib:$OPENBLASROOT/lib/aarch64-linux-gnu:g' configure
  sed -i 's:$OPENBLASROOT/include:$OPENBLASROOT/include/aarch64-linux-gnu:g' configure
  ./configure --mathlib=OPENBLAS --openblas-root=/usr --use-cuda --cudatk-dir=/usr/local/cuda/ --cuda-arch=-arch=sm_53 --shared  #### for jetson nano
  #./configure --mathlib=OPENBLAS --openblas-root=/usr --use-cuda --cudatk-dir=/usr/local/cuda/ --cuda-arch=-arch=sm_72 --shared  #### for jetson xavier nx
  
  sed -i 's: -O1 : -O3 -march=armv8-a+crypto :g' kaldi.mk
  # Jetson nano has small memory(4G) resource, so please use $(nproc) = 1, fortunately it was possible to build natively Kaldi even on jetson nano.
  make depend -j 1
  make -j 1 online2 lm cudadecoder cudadecoderbin  
  
  # prepare datasets on other ubuntu system
  
  mkdir -p ~/workspace/test
  cd ~/workspace/test
  git clone https://github.com/NVIDIA/DeepLearningExamples.git
  cd DeepLearningExamples/Kaldi/SpeechRecognition
  sudo scripts/docker/launch_download.sh
  
  download it on jetson
  
  # run the benchmarking
  
  cp -r ./data /home/revcord/workspace/data/benchmark_data/
  sed -i 's:/data//data//:/home/revcord/workspace/data/wav/benchmark_data/data/:g' /home/revcord/workspace/data/wav/benchmark_data/datasets/LibriSpeech/test_clean/wav_conv.scp
  # copy some of lines to reduce the size of dataset
  
  # check the number of audio files
  wc -l /home/revcord/workspace/data/wav/benchmark_data/datasets/LibriSpeech/test_clean/wav_conv.scp
  
  sed -n '1,1000p' /home/revcord/workspace/data/wav/benchmark_data/datasets/LibriSpeech/test_clean/wav_conv.scp > /home/revcord/workspace/data/wav/benchmark_data/datasets/LibriSpeech/test_clean/wav_conv_sampled.scp
  
  or
  
  awk 'NR>=1 && NR<=500' /home/revcord/workspace/data/wav/benchmark_data/datasets/LibriSpeech/test_clean/wav_conv.scp > /home/revcord/workspace/data/wav/benchmark_data/datasets/LibriSpeech/test_clean/wav_conv_sampled.scp
  
  cd cudadecoderbin
  
   
  /opt/nvidia-kaldi/src/cudadecoderbin/batched-wav-nnet3-cuda-online \
  --frame_subsampling_factor=3 \
  --acoustic-scale=1.0 \
  --max-batch-size=8 \
  --num-channels=64 \
  --beam=10 \
  --lattice-beam=2.0 \
  --max-active=3000 \
  --num-parallel-streaming-channels=8 \
  --write-lattice=true \
  --mfcc-config=/home/revcord/workspace/data/models/LibriSpeech/conf/mfcc.conf \
  --ivector-extraction-config=/home/revcord/workspace/data/models/LibriSpeech/conf/ivector_extractor.conf \
  --word-symbol-table=/home/revcord/workspace/data/models/LibriSpeech/words.txt \
  /home/revcord/workspace/data/models/LibriSpeech/final.mdl \
  /home/revcord/workspace/data/models/LibriSpeech/HCLG.fst \
  scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/LibriSpeech/test_clean/wav_conv_sampled.scp \
  scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp
  
  
  /opt/nvidia-kaldi/src/cudadecoderbin/batched-wav-nnet3-cuda-online \
  --iterations=2 \
  --frame_subsampling_factor=3 \
  --acoustic-scale=1.0 \
  --max-batch-size=8 \
  --num-channels=64 \
  --beam=10 \
  --lattice-beam=2.0 \
  --max-active=3000 \
  --num-parallel-streaming-channels=8 \
  --mfcc-config=/home/revcord/workspace/data/models/LibriSpeech/conf/mfcc.conf \
  --ivector-extraction-config=/home/revcord/workspace/data/models/LibriSpeech/conf/ivector_extractor.conf \
  --word-symbol-table=/home/revcord/workspace/data/models/LibriSpeech/words.txt \
  /home/revcord/workspace/data/models/LibriSpeech/final.mdl \
  /home/revcord/workspace/data/models/LibriSpeech/HCLG.fst \
  scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/LibriSpeech/test_clean/wav_conv_sampled.scp \
  scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp
  
  V1Speech
  
  /opt/nvidia-kaldi/src/cudadecoderbin/batched-wav-nnet3-cuda-online \
  --iterations=1 \
  --frame_subsampling_factor=3 \
  --acoustic-scale=1.0 \
  --max-batch-size=16 \
  --num-channels=64 \
  --beam=10 \
  --lattice-beam=2.0 \
  --max-active=3000 \
  --num-parallel-streaming-channels=8 \
  --mfcc-config=/home/revcord/workspace/data/models/V1Speech/conf/mfcc.conf \
  --ivector-extraction-config=/home/revcord/workspace/data/models/V1Speech/ivector/ivector.conf \
  --word-symbol-table=/home/revcord/workspace/data/models/V1Speech/graph/words.txt \
  /home/revcord/workspace/data/models/V1Speech/am/final.mdl \
  /home/revcord/workspace/data/models/V1Speech/graph/HCLG.fst \
  scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/LibriSpeech/test_clean/wav_conv_sampled.scp \
  scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp   
  
  
  AspireSpeech/HCLG
  
  /opt/kaldi/src/revcordserverbin/revcord-stt-libwebsockets-cuda-online \
  --iterations=1 \
  --frame-subsampling-factor=3 \
  --acoustic-scale=1.0 \
  --max-batch-size=16 
  --xvector-batch-size=32 \
  --num-channels=128 \
  --beam=10 \
  --lattice-beam=2.0 \
  --max-active=3000 \
  --num-parallel-streaming-channels=16 \
  --mfcc-config=/home/revcord/workspace/data/models/AspireSpeech/mfcc.conf \
  --ivector-extraction-config=/home/revcord/workspace/data/models/AspireSpeech/ivector/ivector.conf \
  --word-symbol-table=/home/revcord/workspace/data/models/AspireSpeech/words.txt \
  /home/revcord/workspace/data/models/AspireSpeech/final.mdl \
  /home/revcord/workspace/data/models/AspireSpeech/HCLG.fst \
  scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/AspireSpeech/test_clean/wav_conv_sampled.scp \
  scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp \
  /home/revcord/workspace/data/models/sdmodel
  
    
  Using HCLG.fst, not cuda.fst
  
 /opt/kaldi/src/cudadecoderbin/batched-wav-nnet3-cuda-online --iterations=1 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=12 --num-channels=128 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=8 --xvector-batch-size=32 --mfcc-config=/home/revcord/workspace/data/models/AspireSpeech/mfcc.conf --ivector-extraction-config=/home/revcord/workspace/data/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=/home/revcord/workspace/data/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print-partial-hypotheses /home/revcord/workspace/data/models/AspireSpeech/final.mdl /home/revcord/workspace/data/models/AspireSpeech/HCLG.fst scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/AspireSpeech/test_clean/wav_conv_sampled.scp scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp /home/revcord/workspace/data/models/sdmodel
 
 
  
 /opt/kaldi/src/cudadecoderbin/batched-wav-nnet3-cuda-online --iterations=1 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=14 --num-channels=128 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=14 --xvector-batch-size=32 --mfcc-config=/home/revcord/workspace/data/models/AspireSpeech/mfcc.conf --ivector-extraction-config=/home/revcord/workspace/data/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=/home/revcord/workspace/data/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print-partial-hypotheses /home/revcord/workspace/data/models/AspireSpeech/final.mdl /home/revcord/workspace/data/models/AspireSpeech/HCLG.fst scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/AspireSpeech/test_clean/wav_conv_sampled.scp scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp /home/revcord/workspace/data/models/sdmodel
 
 
 revcord-stt-libwebsockets-cuda-online Using HCLG.fst, not cuda.fst
 
 /opt/kaldi/src/revcordserverbin/revcord-stt-libwebsockets-cuda-online --iterations=1 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=12 --num-channels=128 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=12 --xvector-batch-size=32 --mfcc-config=/home/revcord/workspace/data/models/AspireSpeech/mfcc.conf --ivector-extraction-config=/home/revcord/workspace/data/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=/home/revcord/workspace/data/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print-partial-hypotheses /home/revcord/workspace/data/models/AspireSpeech/final.mdl /home/revcord/workspace/data/models/AspireSpeech/HCLG.fst scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/AspireSpeech/test_clean/wav_conv_sampled.scp scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp /home/revcord/workspace/data/models/sdmodel
 
 
 /opt/kaldi/src/cudadecoderbin/batched-wav-nnet3-cuda-online --iterations=10 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=14 --num-channels=128 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=14 --xvector-batch-size=32 --mfcc-config=/home/revcord/workspace/data/models/AspireSpeech/mfcc.conf --ivector-extraction-config=/home/revcord/workspace/data/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=/home/revcord/workspace/data/models/AspireSpeech/words.txt --generate-lattice --print-hypotheses --print-endpoints --print-partial-hypotheses /home/revcord/workspace/data/models/AspireSpeech/final.mdl /home/revcord/workspace/data/models/AspireSpeech/HCLG.fst scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/AspireSpeech/test_clean/wav_conv.scp scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp /home/revcord/workspace/data/models/sdmodel
 
 /opt/kaldi/src/cudadecoderbin/revcord-stt-libwebsockets-client --iterations=1 --num-parallel-streaming-channels=16  scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/AspireSpeech/test_clean/wav_conv_sampled.scp
 
 
 
 
 ######  For Jetson nano #####  please note --max-mem=250000000 about 1/3 of FST size(AspireSpeech's HCLG = 661604 KB)
 
 -------  Server:
 
 /opt/kaldi/src/revcordserverbin/revcord-stt-libwebsockets-cuda-online --iterations=1 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=8 --num-channels=16 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=8 --xvector-batch-size=32 --mfcc-config=/home/revcord/workspace/data/models/AspireSpeech/mfcc.conf --ivector-extraction-config=/home/revcord/workspace/data/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=/home/revcord/workspace/data/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print-partial-hypotheses /home/revcord/workspace/data/models/AspireSpeech/final.mdl /home/revcord/workspace/data/models/AspireSpeech/HCLG.fst scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/AspireSpeech/test_clean/wav_conv_sampled.scp scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp /home/revcord/workspace/data/models/sdmodel
 
 -----    Benchmark client
 
 Batched-Wav-Cuda-Online ws://192.168.1.21:2571 D:/SpeechBenchmark/datasets/AspireSpeech/test_clean/wav_conv.scp 8 8 100
 
 
 -----  Benchmark in pure c++.
 
 /opt/kaldi/src/cudadecoderbin/batched-wav-nnet3-cuda-online --iterations=1 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=8 --num-channels=32 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=8 --xvector-batch-size=32 --mfcc-config=/home/revcord/workspace/data/models/AspireSpeech/mfcc.conf --ivector-extraction-config=/home/revcord/workspace/data/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=/home/revcord/workspace/data/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print-partial-hypotheses /home/revcord/workspace/data/models/AspireSpeech/final.mdl /home/revcord/workspace/data/models/AspireSpeech/HCLG.fst scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/AspireSpeech/test_clean/wav_conv.scp scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp /home/revcord/workspace/data/models/sdmodel
 
 
 ######  For Jetson Xavier #####  please note --max-mem=250000000 about 1/3 of FST size(AspireSpeech's HCLG = 661604 KB)
 
 -------  Server:
 
 /home/revcord/workspace/kaldi/src/revcordserverbin/revcord-stt-libwebsockets-cuda-online --iterations=1 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=32 --num-channels=64 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=32 --xvector-batch-size=32 --mfcc-config=/home/revcord/workspace/data/models/AspireSpeech/mfcc.conf --ivector-extraction-config=/home/revcord/workspace/data/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=/home/revcord/workspace/data/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print-partial-hypotheses /home/revcord/workspace/data/models/AspireSpeech/final.mdl /home/revcord/workspace/data/models/AspireSpeech/HCLG.fst scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/AspireSpeech/test_clean/wav_conv.scp scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp /home/revcord/workspace/data/models/sdmodel
 
 
 -----    Benchmark client
 
 Batched-Wav-Cuda-Online ws://192.168.1.22:2571 D:/SpeechBenchmark/datasets/AspireSpeech/test_clean/wav_conv.scp 50 50 1
 
 -----  Benchmark in pure c++.
 
 /home/revcord/workspace/kaldi/src/cudadecoderbin/batched-wav-nnet3-cuda-online --iterations=10 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=64 --num-channels=128 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=64 --xvector-batch-size=32 --mfcc-config=/home/revcord/workspace/data/models/AspireSpeech/mfcc.conf --ivector-extraction-config=/home/revcord/workspace/data/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=/home/revcord/workspace/data/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print-partial-hypotheses /home/revcord/workspace/data/models/AspireSpeech/final.mdl /home/revcord/workspace/data/models/AspireSpeech/HCLG.fst scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/AspireSpeech/test_clean/wav_conv.scp scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp /home/revcord/workspace/data/models/sdmodel
 
 