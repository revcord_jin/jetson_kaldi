batched-wav-nnet3-cuda-online --iterations=1 --frame_subsampling_factor=3 --acoustic-scale=1.0 --max-batch-size=32 --num-channels=128 --beam=10 --lattice-beam=2.0 --max-active=3000 --num-parallel-streaming-channels=32 --mfcc-config=F:/SpeechBenchmark/models/LibriSpeech/conf/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/LibriSpeech/conf/ivector_extractor.conf --word-symbol-table=F:/SpeechBenchmark/models/LibriSpeech/words.txt --print-hypotheses --generate-lattice F:/SpeechBenchmark/models/LibriSpeech/final.mdl F:/SpeechBenchmark/models/LibriSpeech/HCLG.fst scp:F:/SpeechBenchmark/datasets/LibriSpeech/test_clean/wav_conv_sampled.scp  scp:F:/SpeechBenchmark/results/test_clean.scp F:/SpeechBenchmark/models/sdmodel

Total Audio: 19452.5 seconds, Total Time: 87.1257 seconds, RTFX: 223.269
LOG (batched-wav-nnet3-cuda-online[5.5-win]:main():cudadecoderbin\batched-wav-nnet3-cuda-online.cc:271)         Latency stats:
LOG (batched-wav-nnet3-cuda-online[5.5-win]:kaldi::cuda_decoder::PrintLatencyStats():cudadecoderbin\cuda-bin-tools.h:49) Latencies (s): Avg             90%             95%             99%
LOG (batched-wav-nnet3-cuda-online[5.5-win]:kaldi::cuda_decoder::PrintLatencyStats():cudadecoderbin\cuda-bin-tools.h:50)                        0.408           0.564           0.611           0.675







/opt/nvidia-kaldi/src/cudadecoderbin/batched-wav-nnet3-cuda-online \
  --iterations=1 \
  --frame_subsampling_factor=3 \
  --acoustic-scale=1.0 \
  --max-batch-size=11 \
  --num-channels=64 \
  --beam=10 \
  --lattice-beam=2.0 \
  --max-active=3000 \
  --num-parallel-streaming-channels=11 \
  --xvector-batch-size=32 \
  --mfcc-config=/home/revcord/workspace/data/models/LibriSpeech/conf/mfcc.conf \
  --ivector-extraction-config=/home/revcord/workspace/data/models/LibriSpeech/conf/ivector_extractor.conf \
  --word-symbol-table=/home/revcord/workspace/data/models/LibriSpeech/words.txt \
  --cuda-fst-rxfilename=/home/revcord/workspace/data/models/LibriSpeech/cuda.fst \
  /home/revcord/workspace/data/models/LibriSpeech/final.mdl \
  /home/revcord/workspace/data/models/LibriSpeech/HCLG.fst \
  scp:/home/revcord/workspace/data/wav/benchmark_data/datasets/LibriSpeech/test_clean/wav_conv_sampled.scp \
  scp:/home/revcord/workspace/data/wav/benchmark_data/results/test_clean.scp
  
  Aspire Model testing ....
  
batched-wav-nnet3-cuda-online --iterations=1 --frame_subsampling_factor=3 --acoustic-scale=1.0 --max-batch-size=256 --num-channels=512 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=256 --xvector-batch-size=32 --mfcc-config=F:/SpeechBenchmark/models/AspireSpeech/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=F:/SpeechBenchmark/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print_partial_hypotheses F:/SpeechBenchmark/models/AspireSpeech/final.mdl F:/SpeechBenchmark/models/AspireSpeech/HCLG.fst scp:F:/SpeechBenchmark/datasets/AspireSpeech/test_clean/wav_conv.scp  scp:F:/SpeechBenchmark/results/test_clean.scp F:/SpeechBenchmark/models/sdmodel 


LibriSpeech Model testing...

batched-wav-nnet3-cuda-online --iterations=1 --frame_subsampling_factor=3 --acoustic-scale=1.0 --max-batch-size=256 --num-channels=512 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=256 --xvector-batch-size=32 --mfcc-config=F:/SpeechBenchmark/models/LibriSpeech/conf/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/LibriSpeech/conf/ivector_extractor.conf --word-symbol-table=F:/SpeechBenchmark/models/LibriSpeech/words.txt --print-hypotheses --generate-lattice F:/SpeechBenchmark/models/LibriSpeech/final.mdl F:/SpeechBenchmark/models/LibriSpeech/HCLG.fst scp:F:/SpeechBenchmark/datasets/LibriSpeech/test_clean/wav_conv_sampled.scp  scp:F:/SpeechBenchmark/results/test_clean.scp F:/SpeechBenchmark/models/sdmodel


long wav-nnet3-cuda-online

batched-wav-nnet3-cuda-online --iterations=1 --frame_subsampling_factor=3 --acoustic-scale=1.0 --max-batch-size=64 --num-channels=128 --beam=10 --lattice-beam=2.0 --max-active=3000 --num-parallel-streaming-channels=64 --xvector-batch-size=64 --mfcc-config=F:/SpeechBenchmark/models/AspireSpeech/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=F:/SpeechBenchmark/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints F:/SpeechBenchmark/models/AspireSpeech/final.mdl F:/SpeechBenchmark/models/AspireSpeech/HCLG.fst scp:E:/tmp/batched/test_bill/wav_conv_bill.scp  scp:F:/SpeechBenchmark/results/test_clean.scp F:/SpeechBenchmark/models/sdmodel




kaldi-cuda-server test

RevcordSTTWSServer.exe --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=256 --num-channels=512 --beam=10 --lattice-beam=2.0 --max-active=3000 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --xvector-batch-size=64 --mfcc-config=F:/SpeechBenchmark/models/AspireSpeech/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=F:/SpeechBenchmark/models/AspireSpeech/words.txt F:/SpeechBenchmark/models/AspireSpeech/final.mdl F:/SpeechBenchmark/models/AspireSpeech/HCLG.fst F:/SpeechBenchmark/models/sdmodel


kaldi-cuda-server-test --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=256 --num-channels=512 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=256 --xvector-batch-size=32 --mfcc-config=F:/SpeechBenchmark/models/AspireSpeech/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=F:/SpeechBenchmark/models/AspireSpeech/words.txt  F:/SpeechBenchmark/models/AspireSpeech/final.mdl F:/SpeechBenchmark/models/AspireSpeech/HCLG.fst  F:/SpeechBenchmark/models/sdmodel 

batched-wav-nnet3-cuda-online --iterations=1 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=256 --num-channels=512 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=256 --xvector-batch-size=32 --mfcc-config=F:/SpeechBenchmark/models/AspireSpeech/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=F:/SpeechBenchmark/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print_partial_hypotheses F:/SpeechBenchmark/models/AspireSpeech/final.mdl F:/SpeechBenchmark/models/AspireSpeech/HCLG.fst scp:F:/SpeechBenchmark/datasets/AspireSpeech/test_clean/wav_conv.scp scp:F:/SpeechBenchmark/results/test_clean.scp F:/SpeechBenchmark/models/sdmodel

kaldi-cuda-server-test --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=256 --num-channels=512 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=256 --xvector-batch-size=32 --mfcc-config=F:/SpeechBenchmark/models/AspireSpeech/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=F:/SpeechBenchmark/models/AspireSpeech/words.txt  F:/SpeechBenchmark/models/AspireSpeech/final.mdl F:/SpeechBenchmark/models/AspireSpeech/HCLG.fst F:/SpeechBenchmark/models/sdmodel


RevcordSTTWSServer.exe --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=256 --num-channels=512 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=256 --xvector-batch-size=64 --mfcc-config=F:/SpeechBenchmark/models/AspireSpeech/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=F:/SpeechBenchmark/models/AspireSpeech/words.txt  F:/SpeechBenchmark/models/AspireSpeech/final.mdl F:/SpeechBenchmark/models/AspireSpeech/HCLG.fst F:/SpeechBenchmark/models/sdmodel


Batched-Wav-Without-Websocket.exe Batched-Wav-Without-Websocket.exe --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=256 --num-channels=512 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=256 --xvector-batch-size=32 --mfcc-config=F:/SpeechBenchmark/models/AspireSpeech/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=F:/SpeechBenchmark/models/AspireSpeech/words.txt  F:/SpeechBenchmark/models/AspireSpeech/final.mdl F:/SpeechBenchmark/models/AspireSpeech/HCLG.fst F:/SpeechBenchmark/models/sdmodel

#######  just one wav file scp:F:/SpeechBenchmark/datasets/AspireSpeech/test_clean/wav_conv_bill.scp  ############

batched-wav-nnet3-cuda-online --iterations=1 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=256 --num-channels=1024 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=256 --xvector-batch-size=32 --mfcc-config=F:/SpeechBenchmark/models/AspireSpeech/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=F:/SpeechBenchmark/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print_partial_hypotheses F:/SpeechBenchmark/models/AspireSpeech/final.mdl F:/SpeechBenchmark/models/AspireSpeech/HCLG.fst scp:F:/SpeechBenchmark/datasets/AspireSpeech/test_clean/wav_conv.scp scp:F:/SpeechBenchmark/results/test_clean.scp F:/SpeechBenchmark/models/sdmodel



#######  libwebsockets  ##########  please note --max-mem=250000000 about 1/3 of FST size(AspireSpeech's HCLG = 661604 KB)

revcord-stt-libwebsockets-cuda-online --iterations=1 --frame-subsampling-factor=3 --acoustic-scale=1.0 --max-batch-size=256 --num-channels=1024 --beam=10 --lattice-beam=2.0 --endpoint.silence-phones=1:2:3:4:5:6:7:8:9:10 --endpoint.rule2.min-trailing-silence=0.5 --endpoint.rule3.min-trailing-silence=1.0 --endpoint.rule4.min-trailing-silence=2.0 --max-active=3000 --num-parallel-streaming-channels=256 --max-mem=250000000 --xvector-batch-size=32 --mfcc-config=F:/SpeechBenchmark/models/AspireSpeech/mfcc.conf --ivector-extraction-config=F:/SpeechBenchmark/models/AspireSpeech/ivector/ivector.conf --word-symbol-table=F:/SpeechBenchmark/models/AspireSpeech/words.txt --print-hypotheses --generate-lattice --print-endpoints --print_partial_hypotheses F:/SpeechBenchmark/models/AspireSpeech/final.mdl F:/SpeechBenchmark/models/AspireSpeech/HCLG.fst scp:F:/SpeechBenchmark/datasets/AspireSpeech/test_clean/wav_conv.scp scp:F:/SpeechBenchmark/results/test_clean.scp F:/SpeechBenchmark/models/sdmodel

Batched-Wav-Cuda-Online ws://localhost:2571 F:/SpeechBenchmark/datasets/AspireSpeech/test_clean/wav_conv.scp 256 150 200000 > e:/tmp/batch.txt


#######  GTX 1060 6GB share a unified address space with the host.
#######
#######  So, we must use cudaHostRegisterPortable NOT cudaHostRegisterDefault to make pinned memory for h_all_wavform 
#######  in cudaHostRegister(h_all_waveform_.Data(), h_all_waveform_.SizeInBytes(), ?); cudaHostRegisterPortable);// , cudaHostRegisterDefault);
#######
